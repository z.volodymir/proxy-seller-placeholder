import React, { Suspense } from 'react'
import { Container, Spinner } from 'react-bootstrap'
import { Outlet } from 'react-router-dom'

const Layout = () => {
  return (
    <main className="d-flex align-items-center min-vh-100">
      <Container>
        <Suspense
          fallback={
            <center>
              <Spinner animation="border" variant="primary" />
            </center>
          }
        >
          <Outlet />
        </Suspense>
      </Container>
    </main>
  )
}

export default Layout
