const BASE_URL = 'https://jsonplaceholder.typicode.com/'
const USERS = `${BASE_URL}/users`

export const getUsers = async () => {
  const response = await fetch(USERS)

  if (!response.ok) {
    throw new Error(
      `Something went wrong!!! Status code ${response.statusCode}`,
    )
  }

  return response.json()
}

export const getContent = async (content) => {
  const response = await fetch(`${USERS}/${content}`)

  if (!response.ok) {
    throw new Error(
      `Something went wrong!!! Status code ${response.statusCode}`,
    )
  }

  return response.json()
}
