import React, { useEffect, useState } from 'react'
import { ListGroup } from 'react-bootstrap'
import { getUsers } from '../api'
import UserItem from './UserItem'

const UserList = () => {
  const [users, setUsers] = useState([])

  useEffect(() => {
    getUsers()
      .then((response) => setUsers(response))
      .catch((error) => console.log(error.message))
  }, [])

  return (
    <ListGroup as="ul">
      {users.length > 0 &&
        users.map((user) => (
          <UserItem
            key={user.id}
            id={user.id}
            name={user.name}
            username={user.username}
            email={user.email}
          />
        ))}
    </ListGroup>
  )
}

export default UserList
