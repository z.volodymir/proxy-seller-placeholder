import React, { memo } from 'react'
import { Link } from 'react-router-dom'
import { Button, ButtonGroup, ListGroupItem, Stack } from 'react-bootstrap'

const UserItem = memo(({ id, name, username, email }) => {
  return (
    <ListGroupItem
      as="li"
      className="d-flex flex-column flex-md-row align-items-center justify-content-between"
    >
      <Stack gap={2}>
        <p className="m-0">
          <b>Name:</b> {name}
        </p>
        <p className="m-0">
          <b>User Name:</b> {username}
        </p>
        <p className="m-0">
          <b>Email:</b> {email}
        </p>
      </Stack>
      <ButtonGroup className="mt-2 mt-md-0">
        <Button variant="secondary">
          <Link
            to={`users/${id}/albums`}
            className="link-dark text-decoration-none"
          >
            Albums
          </Link>
        </Button>
        <Button variant="outline-secondary">
          <Link
            to={`users/${id}/posts`}
            className="link-dark text-decoration-none"
          >
            Posts
          </Link>
        </Button>
      </ButtonGroup>
    </ListGroupItem>
  )
})

export default UserItem
