import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, ListGroup, Spinner, Stack } from 'react-bootstrap'
import { getContent } from '../api'
import ContentItem from './ContentItem'

const Posts = () => {
  const [posts, setPosts] = useState([])
  const params = useParams()
  const navigate = useNavigate()

  useEffect(() => {
    const path = `${params.id}/posts`
    getContent(path)
      .then((response) => setPosts(response))
      .catch((error) => console.log(error.message))
  }, [])

  return (
    <>
      {!posts.length > 0 ? (
        <center>
          <Spinner animation="border" variant="primary" />
        </center>
      ) : (
        <Stack gap={3}>
          <Stack
            direction="horizontal"
            gap={3}
            className="justify-content-center"
          >
            <h1 className="text-center">Posts</h1>
            <Button variant="info" onClick={() => navigate('..')}>
              Back
            </Button>
          </Stack>
          <ListGroup as="ul">
            {posts.map((post) => (
              <ContentItem
                key={post.id}
                title={post.title}
                body={post.body}
                context="posts"
              />
            ))}
          </ListGroup>
        </Stack>
      )}
    </>
  )
}

export default Posts
