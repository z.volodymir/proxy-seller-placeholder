import React, { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom'
import { ListGroup, Stack } from 'react-bootstrap'
import { getUsers } from '../api'
import { filterUser, sortKeys, sortUsers } from '../utils'
import UserItem from './UserItem'
import UsersSort from './UsersSort'
import UsersFilter from './UsersFilter'

const Users = () => {
  const [searchParams, setSearchParams] = useSearchParams()

  const [users, setUsers] = useState([])
  const [sortedUsers, setSortedUsers] = useState([])
  const [sortType, setSortType] = useState('')
  const [filter, setFilter] = useState('')

  useEffect(() => {
    getUsers()
      .then((response) => {
        setUsers(response)
        setSortedUsers(response)
      })
      .catch((error) => console.log(error.message))
  }, [])

  useEffect(() => {
    if (sortType?.trim() && sortKeys.includes(sortType)) {
      const type = sortType.trim().toLowerCase()

      setSortedUsers(sortUsers(sortedUsers, type))

      searchParams.set('sort', type)
      setSearchParams(searchParams)
    } else {
      setSortedUsers(users)
    }
  }, [users, sortType])

  useEffect(() => {
    const sortParam = searchParams.get('sort')

    if (sortParam && sortKeys.includes(sortParam)) {
      const type = sortParam.toLowerCase()

      setSortType(type)
      setSortedUsers(sortUsers(sortedUsers, type))
    }
  }, [users, searchParams.get('sort')])

  useEffect(() => {
    const filterParam = searchParams.get('filter')

    if (filterParam && users.length > 0) {
      setFilter(filterParam)

      setSortedUsers(filterUser(sortUsers(users, sortType), filterParam))
    }
  }, [users, sortType, searchParams.get('filter')])

  return (
    <Stack gap={3}>
      <h1 className="text-center">Users</h1>
      <Stack direction="horizontal" gap={3} className="justify-content-center">
        <UsersSort sortType={sortType} setSortType={setSortType} />
        <UsersFilter
          users={users}
          setSortedUsers={setSortedUsers}
          filter={filter}
          setFilter={setFilter}
        />
      </Stack>
      <ListGroup as="ul">
        {sortedUsers.length > 0 &&
          sortedUsers.map((user) => (
            <UserItem
              key={user.id}
              id={user.id}
              name={user.name}
              username={user.username}
              email={user.email}
            />
          ))}
      </ListGroup>
    </Stack>
  )
}

export default Users
