import React, { useEffect } from 'react'
import { Form } from 'react-bootstrap'
import { useSearchParams } from 'react-router-dom'
import { filterUser } from '../utils'

const UsersFilter = ({ users, setSortedUsers, filter, setFilter }) => {
  const [searchParams, setSearchParams] = useSearchParams()

  useEffect(() => {
    if (filter.trim()) {
      setSortedUsers(filterUser(users, filter))

      searchParams.set('filter', filter.toLowerCase())
      setSearchParams(searchParams)
    } else {
      setSortedUsers(users)
    }
  }, [filter])

  const handleChange = (event) => {
    const target = event.target.value
    setFilter(target)

    if (filter.trim()) {
      searchParams.delete('filter')
      setSearchParams(searchParams)
    }
  }

  return (
    <Form>
      <Form.Label htmlFor="filter">Filter by username</Form.Label>
      <Form.Control id="filter" value={filter} onChange={handleChange} />
    </Form>
  )
}

export default UsersFilter
