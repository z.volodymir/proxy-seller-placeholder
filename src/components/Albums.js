import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, ListGroup, Spinner, Stack } from 'react-bootstrap'
import { getContent } from '../api'
import ContentItem from './ContentItem'

const Albums = () => {
  const [albums, setAlbums] = useState([])
  const params = useParams()
  const navigate = useNavigate()

  useEffect(() => {
    const path = `${params.id}/albums`
    getContent(path)
      .then((response) => setAlbums(response))
      .catch((error) => console.log(error.message))
  }, [])

  return (
    <>
      {!albums.length > 0 ? (
        <center>
          <Spinner animation="border" variant="primary" />
        </center>
      ) : (
        <Stack gap={3}>
          <Stack
            direction="horizontal"
            gap={3}
            className="justify-content-center"
          >
            <h1 className="text-center">Albums</h1>
            <Button variant="info" onClick={() => navigate('..')}>
              Back
            </Button>
          </Stack>
          <ListGroup as="ul">
            {albums.map((album) => (
              <ContentItem
                key={album.id}
                title={album.title}
                body={album.body}
                context="albums"
              />
            ))}
          </ListGroup>
        </Stack>
      )}
    </>
  )
}

export default Albums
