import React, { memo } from 'react'
import { ListGroupItem, Stack } from 'react-bootstrap'

const ContentItem = memo(({ title, body, context }) => {
  return (
    <ListGroupItem
      as="li"
      className="d-flex flex-column flex-md-row align-items-center justify-content-between"
    >
      <Stack gap={3}>
        <h2>{title}</h2>
        {context === 'posts' && <p>{body}</p>}
      </Stack>
    </ListGroupItem>
  )
})

export default ContentItem
