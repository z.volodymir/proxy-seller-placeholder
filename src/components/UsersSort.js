import React, { useEffect, useRef } from 'react'
import { Form, FormLabel, FormSelect } from 'react-bootstrap'
import { useSearchParams } from 'react-router-dom'
import { sortKeys } from '../utils'

const UsersSort = ({ setSortType }) => {
  const [searchParams, setSearchParams] = useSearchParams()

  const selectRef = useRef(null)

  const handleChange = (event) => {
    if (event.target.value) {
      setSortType(event.target.value)
    } else {
      setSortType('')
      searchParams.delete('sort')
      setSearchParams(searchParams)
    }
  }

  useEffect(() => {
    const sortParam = searchParams.get('sort')

    if (sortParam && sortKeys.includes(sortParam)) {
      selectRef.current.value = sortParam
    }
  }, [selectRef.current])

  return (
    <Form>
      <FormLabel>Sort by username</FormLabel>
      <FormSelect
        aria-label="Sort users"
        onChange={handleChange}
        ref={selectRef}
      >
        <option value="">Select type</option>
        {sortKeys.map((key) => (
          <option key={key} value={key}>
            {key}
          </option>
        ))}
      </FormSelect>
    </Form>
  )
}

export default UsersSort
