export const sortUsers = (users, type) => {
  if (type) {
    return [...users].sort((first, second) => {
      if (type === 'asc') {
        return first.username
          .toLowerCase()
          .localeCompare(second.username.toLowerCase())
      }
      return second.username
        .toLowerCase()
        .localeCompare(first.username.toLowerCase())
    })
  }
  return users
}

export const filterUser = (users, filter) => {
  return users.filter((user) =>
    user.username.toLowerCase().includes(filter.toLowerCase()),
  )
}

export const sortKeys = ['asc', 'desc']
