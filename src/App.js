import React, { lazy } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Layout from './layouts/Layout'

const UsersPage = lazy(() => import('./components/Users'))
const AlbumsPage = lazy(() => import('./components/Albums'))
const PostsPage = lazy(() => import('./components/Posts'))
const NotFoundPage = lazy(() => import('./components/NotFound'))

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<UsersPage />} />
          <Route path="users/:id/albums" element={<AlbumsPage />} />
          <Route path="users/:id/posts" element={<PostsPage />} />
          <Route path="*" element={<NotFoundPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
